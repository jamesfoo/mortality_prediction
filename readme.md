# Mortality prediction using ICU clinical data
## 1. Overview
Accurate knowledge of a patient's condition is critical. Electronic monitoring systems and health records provide rich information for performing predictive analytics. In this homework, you will use ICU clinical data to predict the mortality of patients in one month after discharge.

It is your responsibility to make sure that all code and other deliverables are in the correct format and that your submission compiles and runs.  We will not manually check your code. Thus non-runnable code will directly lead to 0 score.


## 2. About the data
there are two CSV files which will be the input data in this assignment.
The data provided in *events.csv* are event sequences. Each line of this file consists of a tuple with the format *(patient_id, event_id, event_description, timestamp, value)*.


+ **patient_id**: Identifies the patients in order to differentiate them from others. For example, the patient in the example above has patient id 1053.
+ **event_id**: Encodes all the clinical events that a patient has had. For example, DRUG19122121 means that a drug with RxNorm code as 19122121 was prescribed to the patient. DIAG319049 means the patient was diagnosed of disease with SNOMED code of 319049 and LAB3026361 means that the laboratory test with a LOINC code of 3026361 was performed on the patient.
+ **event_description**: Shows the description of the event. For example, DIAG319049 is the code for Acute respiratory failure and DRUG19122121 is the code for Insulin.
+ **timestamp**: Indicates the date at which the event happened. Here the timestamp is not a real date but a shifted date for protecting privacy of patients.
+ **value**: Contains the value associated to an event.

The data provided in *mortality_events.csv* contains the patient ids of only the deceased people. They are in the form of a tuple with the format *(patient_id, timestamp, label)*. For example,

| event type    | sample event_id  | value meaning | example  |
| ------------- | ------------- | ----- | ----- |
| diagnostic code     | DIAG319049 | diagnosed with a certain disease, value always be 1.0| 1.0 |
| drug consumption      | DRUG19122121      |   prescribed a certain medication, value will always be 1.0 | 1.0 |
| laboratory test | LAB3026361      |   test conducted on a patient and its value | 3.69 |


## 3. HIVE: Descriptive Statistics
Computing statistics on the data aids in developing predictive models. In this homework, you need to write HIVE code that computes various metrics on the data.

The definition of terms used in the result table are described below:

+ **Event count**: Number of events recorded for a given patient. Note that every line in the input file is an event.
+ **Encounter count**: Count of unique dates on which a given patient visited the ICU.
+ **Record length**: Duration (in number of days) between first event and last event for a given patient.
+ **Common Diagnosis**: 5 Most frequently occurring disease.
+ **Common Laboratory Test**: 5 Most frequently conducted test.
+ **Common Medication**: 5 Most frequently prescribed medication.


While counting common diagnoses, lab tests and medications, count all the occurrences of the codes. e.g. if one patient has the same code 3 times, the total count on that code should include all 3. Furthermore, the count is not per patient but per code.


## 4.Pig : Data transformation
we will convert the raw data to standardized format using Pig. Diagnostic, medication and laboratory codes for each patient should be used to construct the feature vector and the feature vector should be represented in *SVMLight* format.

Listed below are a few concepts you need to know before beginning feature construction (for details please refer to lectures).

+ **Observation Window**: The time interval you will use to identify relevant events. Only events present in this window should be included while constructing  feature vectors. The size of observation window is 2000 days(including 2000).
+ **Prediction Window**: A fixed time interval that is to be used to make the prediction. Events in this interval should not be included while constructing feature vectors. The size of prediction window is 30 days.
+ **Index date**: The day on which mortality is to be predicted. Event occured at index date should be considered within observation window. Index date is evaluated as follows:

  - For deceased patients: Index date is 30 days prior to the death date (timestamp field) in *mortality.csv*.
  - For alive patients: Index date is the last event date in *events.csv* for each alive patient.


## 5. Hadoop: training multiple classifiers in parallel
In this problem, you are going to train multiple logistic regression classifiers with Hadoop in parallel.
